// Realtime Database

// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js';

import { getDatabase, push, ref, onValue } from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-database.js';

// Przygtowanie konfiguracji projektu Firebase'a
const firebaseConfig = {
	apiKey: 'AIzaSyCHbA0BhUPoaMT5INv0cy4mVQmDXRJOUi0',
	authDomain: 'fir-animals-4ae13.firebaseapp.com',
	databaseURL: 'https://fir-animals-4ae13-default-rtdb.europe-west1.firebasedatabase.app',
	projectId: 'fir-animals-4ae13',
	storageBucket: 'fir-animals-4ae13.appspot.com',
	messagingSenderId: '918317747855',
	appId: '1:918317747855:web:1903e31daabdd5ffdc6afa',
};

// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchomienie modułu realtime database
const database = getDatabase(app);

// Definiowanie elementów UI
const nameInput = document.querySelector('#nameInput');
const messageInput = document.querySelector('#messageInput');
const sendMessageBtn = document.querySelector('#sendMessageBtn');
const chatArea = document.querySelector('#chatArea');

// 1. Dodanie możliwości wysyłania wiadomości
const sendMessage = () => {
	//1.1 Pobranie imienia z inputa
	const authorName = nameInput.value;
	//1.2 Pobranie wiadomości z inputa
	const messageName = messageInput.value;

	//1.3 Zapisanie imienia oraz wiadomości w bazie danych
	//push -> dodanie do bazy z automatycznie nadanym kluczem
	//pusch(gdzie?, co(jaki obiekt)?)
	push(ref(database, 'messages'), {
		author: authorName,
		message: messageName,
	});
};

// 1.4 Podpięcie pod przycisk funkcji wysyłającej dane do bazy danych
sendMessageBtn.addEventListener('click', sendMessage);

// 2. Dodanie możliwości odczytu wiadomości
//2.1 Dodanie nasłuchiwania na zmiany pod ścieżką 'messages'
// onValue(na co ma nasłuchiwać?, co robić jak coś się zmieni?)
onValue(ref(database, 'messages'), snapshot => {
	// console.log(snapshot.val());

	//2.2 Wygenerowanie widoku HTML ze wszystkimi wiadomościami
	let chatContent = '';

	Object.values(snapshot.val()).forEach(message => {
		// console.log(message);

		chatContent += `${message.author}: ${message.message}<br>`;
	});

	//2.3 Umieszczenie wiadomości na stronie
	chatArea.innerHTML = chatContent;
});
