// Zadanie Storage- przećwieczenie całego modułu (notatki w archirum)

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js';
import {
	getStorage,
	uploadBytesResumable,
	ref,
	getDownloadURL,
	listAll,
} from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-storage.js';

const firebaseConfig = {
	apiKey: 'AIzaSyCHbA0BhUPoaMT5INv0cy4mVQmDXRJOUi0',
	authDomain: 'fir-animals-4ae13.firebaseapp.com',
	projectId: 'fir-animals-4ae13',
	storageBucket: 'fir-animals-4ae13.appspot.com',
	messagingSenderId: '918317747855',
	appId: '1:918317747855:web:1903e31daabdd5ffdc6afa',
};
//- uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
//- uruchomienie modułu storage
const storage = getStorage(app);

// Definiowanie elementów UI
const imageInput = document.querySelector('#imageInput');
const imageName = document.querySelector('#imageName');
const uploadImageBtn = document.querySelector('#uploadButton');
const uploadImageProgressBar = document.querySelector('#uploadImageProgressBar');
const uploadedImage = document.querySelector('#uploadedImage');

const imagesDropDownList = document.querySelector('#imagesDropDownList');
const selectedImage = document.querySelector('#selectedImage');

const audioPlayer = document.querySelector("#audioPlayer");

// Zadanie 4. Dodaj funkcjonalność, która pozwoli użytkownikowani na dodanie obrazka do
// Storage’a:
// a. Pozwól wybrać użytkownikowi obrazek z dysku lokalnego.
// b. Automatycznie pobieraj obiekt pliku oraz rozszerzenie przesłanego pliku.
// c. Pozwól użytkownikowi na zmianę nazwy pliku przed wrzuceniem na Storage.
// d. Dodaj przycisk, który rozpocznie proces wrzucania pliku do Storage.
// e. W momencie, kiedy przesyłanie się zakończy - wyświetl wrzucony obrazek.

let fileToUpload;
let fileExtension;

imageInput.onchange = event => {
	fileToUpload = event.target.files[0];
	const fullFileName = event.target.files[0].name;
	const splittedFileName = fullFileName.split('.');

	//zad 6
	fileExtension = splittedFileName[splittedFileName.lenght - 1];
};

uploadImageBtn.onclick = () => {
	const fileName = imageName.value;
	const fullFileName = `${fileName}.${fileExtension}`;

	const metaData = {
		contentType: fileToUpload.type,
	};

	const uploadProcess = uploadBytesResumable(ref(storage, `images/${fullFileName}`), fileToUpload, metaData);

	uploadProcess.on(
		'state_changed',
		//Co robić w trakcje przesyłania?
		snapshot => {
			const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
			uploadImageProgressBar.innerHTML = `${Math.round(progress)}%`;
		},
		//Co robić podczas błędu w przesyłaniu?
		error => {
			console.error(error);
		},
		// Co robić, kiedy proces zakończy się sukcesem?
		async () => {
			//3.6 pobranie URL pliku
			const imageUrl = await getDownloadURL(uploadProcess.snapshot.ref);
			// console.log(imageUrl);

			//3.7 Umieszczenie obrazka na stronie
			uploadedImage.setAttribute('src', imageUrl);
		}
	);
};

// Zadanie 5 Dodaj funkcjonalność, która pozwoli wyświetli w UI wszystkie pliki znajdujące się w
// folderze ‘images’ na Storage.

// 5. Pobranie wszystkich dostępnych plików (w folderze)
/*
// 5.1. Pobieranie referencji (wskażników) plików z konkretnego     folderu. Jako wynik otrzymamy tablicę
const allImages = await listAll(ref(storage, 'images'));

// 5.2. Przejście pętlą przez wszystkie znalezione pliki

let allImagesView = "<ul>";

// [ref1, ref2] --> [obietnica, ze dostane link1; obietnica, ze dostane link2]
const getAllImagesPromise = allImages.items.map(async (imageRef) => {
    const imageUrl = await getDownloadURL(imageRef);
    return `<li><img src="${imageUrl}"></li>`;
});

const arrayWithImagesListElements = await Promise.all(getAllImagesPromise);
arrayWithImagesListElements.forEach(imageListElement => {
    allImagesView += imageListElement;
});

allImagesView += "</ul>";

uploadImageProgressBar.innerHTML = allImagesView;
*/

// 6. ** Dodaj zabezpieczenie przed nazwami plików zawierającymi “.” w środku nazwy,
// np. a.b.c.jpg.

//W funkcji onchange (linijka 49.)
// fileExtension = splittedFileName[splittedFileName.lenght - 1];

// 7. ** Z pobranej listy wszystkich plików zrobić drop-down list.
// a. Wyświetl wybrany obrazek z drop-down listy.
// b. Dodaj możliwość usuwania obrazków z Storage’a.

/// 7a
// 7.1 Pobranie wszystkich nazw obrazków
const allImages = await listAll(ref(storage, 'images'));
// 7.2 Utworzenie drop down listy -> HTML
let imagesDropDownListHTML = `<select id="allImagesSelectList">`;

// 7.3 Wrzucenie do listy rozwijanej nazw plików -> HTML
allImages.items.forEach(imageRef => {
	//pobranie nazw wszystkich plikow
	// [a.jpg] -> [a, jpg] -> [0]
	const imageName = imageRef.name.split('.')[0];

	// Wygenerowanie option dla każdego z obrazków
	imagesDropDownListHTML += `<option value="${imageRef.fullPath}">${imageName}</option>`;
});
// 7.4 Zamkniecie listy rozwijanej -> HTML
imagesDropDownListHTML += '</select>';

// 7.5 Umieszczenie listy rozwijanej na stronie
imagesDropDownList.innerHTML = imagesDropDownListHTML;

// 7.6 Dodanie boslugi reagowania na zmiany w rozwijaku
imagesDropDownList.onchange = async event => {
	//7.6.1 Pobranie obrazka ze storage na bazie pełnej ściezli
	const selectedImageFullPath = event.target.value;
	const imageUrl = await getDownloadURL(ref(storage, selectedImageFullPath));
	selectedImage.setAttribute('src', imageUrl);
};


// 8. ** Dodaj możliwość wrzucenia pliku audio oraz odsłuchania tego pliku.

// 8.1. Pobranie linku do pliku audio ze Storage'a
const audioUrl = await getDownloadURL(ref(storage, "audio/audio.mp3"));

// 8.2. Umieszczenie na stronie odtwarzacza audio -> HTML
// 8.3. Podpięcie pod odtwarzacz pobranego pliku audio
let audioPlayerHTML = 
    `<audio controls>
        <source src="${audioUrl}" type="audio/mpeg">
     </audio>
    `;

// 8.4. Umieszczenie odtwarzacza na stronie
audioPlayer.innerHTML = audioPlayerHTML;


// 9. *** Ustaw zasady bezpieczeństwa dla modułu Storage:
// a. Dodaj możliwość wrzucania obrazków do Storage’a tylko zalogowanym użytkownikom
// rules_version = '2';
// service firebase.storage {
//   match /b/{bucket}/o {
//     match /{allPaths=**} {
//       allow read, write: if
//           request.auth != null;
//     }
//   }
// }
