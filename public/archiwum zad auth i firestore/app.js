import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js';
import {
	getFirestore,
	doc,
	getDoc,
	setDoc,
	collection,
	query,
	getDocs,
	addDoc,
	where,
	orderBy,
	updateDoc,
	deleteDoc,
} from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js';

import {
	getAuth,
	signInWithEmailAndPassword,
	createUserWithEmailAndPassword,
	signOut,
	onAuthStateChanged,
	AuthErrorCodes,
	GoogleAuthProvider,
	signInWithPopup,
} from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-auth.js';

const firebaseConfig = {
	apiKey: 'AIzaSyCHbA0BhUPoaMT5INv0cy4mVQmDXRJOUi0',
	authDomain: 'fir-animals-4ae13.firebaseapp.com',
	projectId: 'fir-animals-4ae13',
	storageBucket: 'fir-animals-4ae13.appspot.com',
	messagingSenderId: '918317747855',
	appId: '1:918317747855:web:1903e31daabdd5ffdc6afa',
};
//- uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
//- uruchomienie modułu bazy danych (firestore)
const database = getFirestore(app);
//- uruchoemienie modułu uwierzytelniania
const auth = getAuth(app);

// ---------------------------------------------------------------------
// Logika auth!

const emailForm = document.querySelector('#emailForm');
const passwordForm = document.querySelector('#passwordForm');
const firstNameForm = document.querySelector('#firstNameForm');

const signInBtn = document.querySelector('#signInBtn');
const signUpBtn = document.querySelector('#signUpBtn');
const signOutBtn = document.querySelector('#signOutBtn');
const signInWithGoogleBtn = document.querySelector('#signInWithGoogleBtn');

const errorLabel = document.querySelector('#errorLabel');
const welcomeUser = document.querySelector('#welcomeUser');

const viewForNotLoggedUser = document.querySelector('#viewForNotLoggedUser');
const viewForLoggedUser = document.querySelector('#viewForLoggedUser');

const allAnimalsDiv = document.querySelector('#allAnimals');
const animalNameForm = document.querySelector('#animalNameForm');
const createAnimalBtn = document.querySelector('#createAnimalBtn');

/************************/
//functions about view password

let pwShown = 0;
const eye = document.getElementById('eye');

const show = () => {
	passwordForm.setAttribute('type', 'text');
};
const hide = () => {
	passwordForm.setAttribute('type', 'password');
};

const passShown = () => {
	if (pwShown == 0) {
		pwShown = 1;
		show();
	} else {
		pwShown = 0;
		hide();
	}
};

eye.addEventListener('click', passShown);
/************************/

//Register user

const signUpUser = async () => {
	const valueEmail = emailForm.value;
	const valuePassword = passwordForm.value;

	try {
		const user = await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);
	} catch (error) {
		if (error.code === 'auth/email-already-in-use') {
			errorLabel.innerHTML = 'Select another email. This email is already taken.';
		} else {
			errorLabel.innerHTML = 'Registration failed...';
		}
	}
};

signUpBtn.addEventListener('click', signUpUser);

//Login User

const signInUser = async () => {
	const valueEmail = emailForm.value;
	const valuePassword = passwordForm.value;

	try {
		const user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
	} catch (exception) {
		errorLabel.innerHTML = 'Invalid email or password!';
	}
};

signInBtn.addEventListener('click', signInUser);

// Sing out User
const signOutUser = async () => {
	await signOut(auth);
};

signOutBtn.addEventListener('click', signOutUser);






// Zadanie 10 (auth) Pokaż zalogowanym użytkownikom zawartość kolekcji stworzonej w bazie danych
// w poprzednim module (Firestore Database).
// -> Należy rozpoznać, czy ktoś jest zalogowany
// -> Jeżeli jest to wywołać funkcję, która uzupełni HTML o zawartość kolekcji
// logika w funckji observer

// zadanie 10 funkcja
const generateAllAnimalsView = async () => {
	// Zadanie 10 (auth)
	const allAnimalsQuery = query(collection(database, 'Zwierzeta'));
	const allAnimals = await getDocs(allAnimalsQuery);

	let allAnimalsView = '<ul>';

	allAnimals.forEach(animal => {
		allAnimalsView += `<li>${animal.data().nazwa}</li>`;
	});

	allAnimalsView += '</ul>';

	allAnimalsDiv.innerHTML = allAnimalsView;
};

// 11.(auth) Dodanie możliwości dodawania dokumentu z poziomu UI zalogowanym
// użytkownikom
// -> Należy rozpoznać, czy ktoś jest zalogowany
// -> Należy wyświetlić formularz dodawania nowego zwierzęcia do kolekcji
// stworzonej w poprzednim module. Po naciśnięciu przycisku ‘utwórz’ należy dodać
// rekord do bazy danych oraz odświeżyć listę wszystkich wyświetlanych zwierząt z
// kolekcji z bazy danych.
// -> Skorzystaj z podejścia, w którym ID jest generowane automatycznie przez
// Firebase

const createAnimal = async () => {
	const name = animalNameForm.value;
	const animalsCollectionRef = collection(database, 'Zwierzeta');

	await addDoc(animalsCollectionRef, { nazwa: name });

	generateAllAnimalsView();
};

createAnimalBtn.addEventListener('click', createAnimal);

// 12. * Dodanie możliwości logowanie poprzez konto Google
// -> https://firebase.google.com/docs/auth/web/google-signin?hl=en
// const signInWithGoogle = async () => {
//     const authProvider = new GoogleAuthProvider();
//     const user = await signInWithPopup(auth, authProvider);
//     console.log(user);
// };

// signInWithGoogleBtn.addEventListener("click", signInWithGoogle);












//Watch for a change in user session status
const authUserObserver = () => {
	onAuthStateChanged(auth, async user => {
		if (user) {
			//Ktoś jest zalogowany
			// console.log(';)');
			viewForLoggedUser.style.display = 'block';
			welcomeUser.innerHTML = `Welcome ${firstNameForm.value} 😀`;
			viewForNotLoggedUser.style.display = 'none';

			// zadanie10
			generateAllAnimalsView();
		} else {
			//Ktoś nie jest zalogowany
			// console.log(';<');
			viewForLoggedUser.style.display = 'none';
			viewForNotLoggedUser.style.display = 'block';
		}
	});
};
authUserObserver();

//Operacja 4 Hosting
//Konczysz działanie aplikacji w konsoli crt+c, nastepnie:
// w consoli wpisujesz: firebase deploy

//--------------------------------------------------------

// Logika firestore
console.log('Wyświetlenie użytkowników kolekcji firestore');

const usersCollectionRef = collection(database, 'users');
const queryAllUsers = query(usersCollectionRef);
const allUsersSnap = await getDocs(queryAllUsers);

allUsersSnap.forEach(user => {
	console.log(user.data());
});

// 1. Wyświetlenie informacji na temat zwierzęcia o ID=1
// const animalRef = doc(database, 'Zwierzeta', '1');
// const animalSnap = await getDoc(animalRef);

// if (animalSnap.exists()) {
// 	console.log(animalSnap.data());
// } else {
// 	console.log('Dokument nie istnieje');
// }

// 2. Dopisz funkcjonalność, która wyświetli w konsoli nazwy wszystkich dostępnych zwierząt

// console.log('Nazwy wszystkich dostępnych zwierząt');

// const animalsCollectionRef = collection(database, 'Zwierzeta');

// const queryAllAnimals = query(animalsCollectionRef);
// const allAnimalsSnap = await getDocs(queryAllAnimals);

// allAnimalsSnap.forEach(animal => {
// 	console.log(animal.data().nazwa);
// });

// 3. Dopisz funkcjonalność, która wyświetli w konsoli nazwy wszystkich zwierząt,których adopcja wymaga pozwolenia (wymagane_pozwolenie=true).

// console.log('Zwierzeta ktore wymagaja pozwolenia:');

// const queryPermissionIsRequired = query(animalsCollectionRef, where('wymagane_pozwolenie', '==', true));
// const allAnimalsPermissionIsRequired = await getDocs(queryPermissionIsRequired);

// allAnimalsPermissionIsRequired.forEach(animal => {
// 	console.log(animal.data().nazwa);
// });

//4. Dopisz funkcjonalność, która wyświetli w konsoli nazwy oraz ceny wszystkich dostępnych zwierząt w kolejności od najdroższych do najtańszych.

// console.log('SORTOWANIE: Nazwy oraz ceny wszystkich dostepnych zwierząt od najdroższych do najtańszych');

// const queryAllAnimalsSortedByPriceDescending = query(animalsCollectionRef, orderBy('cena', 'desc'));
// const allAnimalsSortedByPriceDescending = await getDocs(queryAllAnimalsSortedByPriceDescending);

// allAnimalsSortedByPriceDescending.forEach(animal => {
// 	console.log(`${animal.data().nazwa}: ${animal.data().cena} PLN`);
// });

//11. Dopisz funkcjonalność, która doda nowe zwierzę do bazy danych (możesz przyjąć stałe ID). Potwierdzniem poprawnego działania tego punktu będzie pobranie danych z bazy dla tego zwierzęcia oraz wyświetlenie tych danych w konsoli w przeglądarce.
// console.log('Wyświetlenie nowego, dodanego zwierzecia do bazy');
// const setAnimalRef = doc(database, 'Zwierzeta', '4');
// const propertiesOfMouse = {
// 	cena: 300,
// 	nazwa: 'myszka Miki',
// 	wymagane_pozowlenie: true,
// };
// await setDoc(setAnimalRef, propertiesOfMouse);
// const createdAnimal = await getDoc(setAnimalRef, propertiesOfMouse);

// console.log(createdAnimal.data().nazwa);

//12. Dopisz funkcjonalność, która zmodyfikuje cenę zwierzęcia dodanego w poprzednim punkcie. Ze względu na promocję cenę należy obniżyć o 30%.
// Potwierdzniem poprawnego działania tego punktu będzie pobranie danych z bazy dla tego zwierzęcia oraz wyświetlenie tych danych w konsoli w przeglądarce.
// console.log('Obniżenie ceny myszy');

// UpDate: Aktualicowanie danych w bazie danych
//Operacja 6: Aktualizujemy dane w kolkcji

// const productToUpdateRef = doc(database, 'produkty', 'laptop b12');
// const productToUpdateProperties = {
// 	cena: 1200,
// 	czy_dostepny: true,
// };
// await updateDoc(productToUpdateRef, productToUpdateProperties);

// const mouseToUpdateRef = doc(database, 'Zwierzeta', '4');
// const mouseToUpdatePrice = {
// 	cena: 300 * 0.7,
// };
// await updateDoc(mouseToUpdateRef, mouseToUpdatePrice);

// const newPriceOfMouse = await getDoc(mouseToUpdateRef);
// console.log(newPriceOfMouse.data().cena);

// Dopisz funkcjonalność, która usunie zwierzę dodane w punkcie 11.
// Potwierdzniem poprawnego działania tego punktu będzie pobranie listy
// wszystkich zwierząt z danych, wyświetlenie ich nazw oraz sprawdzenie, że
// nazwa usuniętego zwierzęcia już się nie pojawia.

// const animalToDeleteRef = doc(database, 'Zwierzeta', '4');
// await deleteDoc(animalToDeleteRef);

// allAnimalsSnap.forEach(singleAnimal => {
// 	console.log(singleAnimal.data().nazwa);
// });

// 14. * Dopisz funkcjonalność, która doda UI wyświetlający nazwy oraz ceny
// wszystkich zwierząt.
// a. Forma dowolna, może to być lista, może to być tabelka, może być
// cokolwiek innego - decyzja należy do osoby programującej
// funkcjonalność.

// const generateAllAnimalsView = async () => {
// 	console.log('abc');

// Zadanie 14
// 14.1: Pobranie wszystkich zwierząt z bazy danych
// const queryAllAnimals = query(collection(database, 'Zwierzeta'));
// const allAnimals = await getDocs(queryAllAnimals);

// 14.2: Wygenerowanie fragmentu HTML
// let allAnimalsView = '<ol>';

// 16.1 Wyświetlenie ceny w postaci inputa
// allAnimals.forEach(animal => {
// 	allAnimalsView += `<li>
//   ${animal.data().nazwa}:
//   <input type="number" data-id=${animal.id} value=${animal.data().cena} class="inputPriceUpdate">
//   <button type="button" data-id=${animal.id} class="btnDelete">Usuń</button>
//   </li>`;
// });

// allAnimalsView += '</ol>';

// 14.3: Wrzucenie stworzonego widoku na stronę
// const allAnimalsDiv = document.querySelector('#allAnimals');
// allAnimalsDiv.innerHTML = allAnimalsView;

// 15.2.2: Wyłapanie przycisków z HTMLa ✅
// const btnsDelete = document.querySelectorAll('.btnDelete');

// 15.2.4: Dodanie akcji usuwania do przycisków ✅
// btnsDelete.forEach(btn => {
// 	btn.addEventListener('click', deleteAnimalFromDatabse);
// });
// 16.3 Dodanie do inputa eventu, ktory bedzie reagował na zmiane wartosci
//16.3.1 Znalezienie wszystkich inputow

// const inputsUpdate = document.querySelectorAll('.inputPriceUpdate');

//16.3.2 Dodanie eventow do inpputow
// 	inputsUpdate.forEach(input => {
// 		input.addEventListener('change', updatePriceInDatabase);
// 	});
// };

// 15.2.3: Dodanie funkcji odpowiedzialnej za usuwanie zwierząt z bazy ✅
// const deleteAnimalFromDatabse = async event => {
// 	const animalId = event.target.dataset.id;
// 	const animalRef = doc(database, 'Zwierzeta', animalId);
// 	await deleteDoc(animalRef);

// 	generateAllAnimalsView();
// };

// 16.2 Napisanie funkcjonalnosci, ktora aktualizuje cene w bazie
// const updatePriceInDatabase = async event => {
// 	const animalId = event.target.dataset.id;
// 	const animalRef = doc(database, 'Zwierzeta', animalId);
// 	const newPrice = Number(event.target.value);
// 	const updatedAnimal = {
// 		cena: newPrice,
// 	};

// 	await updateDoc(animalRef, updatedAnimal);
// };

// generateAllAnimalsView();

// Zadanie 15
// 15.1: Dodanie przycisku "Usuń" obok ceny ✅

// 15.2: Dodanie do każdego przycisku akcji usuwania danego zwierzęcia

// 15.2.1: Dodanie klasy do przycisków, żeby potem złapać je przez JS ✅

//15.3 Po zakończeniu usuwania należy odświeżyć listę widocznych zwierząt ✅

// 16. *** Dopisz funkcjonalność, która pozwoli na aktualizację ceny zwierzęcia z
// poziomu UI.
// a. Cena zwierzęcia może być trzymana w postaci np. Inputa.
// b. Po zmodyfikowaniu wartości ceny należy zaktualizować wartość w bazie
// danych oraz lista dostępnych zwierząt powinna zostać odświeżona.

//
