import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js';
import {
	getStorage,
	uploadBytesResumable,
	ref,
	getDownloadURL,
	listAll,
} from 'https://www.gstatic.com/firebasejs/9.17.1/firebase-storage.js';

const firebaseConfig = {
	apiKey: 'AIzaSyCHbA0BhUPoaMT5INv0cy4mVQmDXRJOUi0',
	authDomain: 'fir-animals-4ae13.firebaseapp.com',
	projectId: 'fir-animals-4ae13',
	storageBucket: 'fir-animals-4ae13.appspot.com',
	messagingSenderId: '918317747855',
	appId: '1:918317747855:web:1903e31daabdd5ffdc6afa',
};
//- uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
//- uruchomienie modułu bazy danych (firestore)
// const database = getFirestore(app);
//- uruchoemienie modułu uwierzytelniania
// const auth = getAuth(app);

//- uruchomienie modułu storage
const storage = getStorage(app);

//STORAGE

// Definiowanie elementów UI
const imageInput = document.querySelector('#imageInput');
const imageName = document.querySelector('#imageName');
const uploadImageBtn = document.querySelector('#uploadButton');
const uploadImageProgressBar = document.querySelector('#uploadImageProgressBar');
const uploadedImage = document.querySelector('#uploadedImage');

// ----- WRZUCANIE PLIKU Z DYSKU LOKALNEGO NA SERWER -----
// 1. Zmienne pomocnicze do przechowania pliku
let fileToUpload;
let fileExtension;

// 2. Obsługa procesu wyboru pliku
// 2.1. Wbijamy się w moment, kiedy plik zostanie wybrany,
//      więc event 'change' zostanie wywołany
imageInput.onchange = event => {
	// 2.2. Zapamiętanie całego pliku, który wrzucimy na Storage
	fileToUpload = event.target.files[0];

	// 2.3. Pobranie pełnej nazwy wybranego przez użytkownika pliku
	const fullFileName = event.target.files[0].name;

	// 2.4. Podział pełnej nazwy wybranego pliku na nazwę oraz rozszerzenie pliku
	// divi_moduły.jpg -> ["nazwa pliku", "jpg"]
	const splittedFileName = fullFileName.split('.');

	// 2.5. Pobranie samego rozszerzenia pliku
	fileExtension = splittedFileName[1];
};

// 3. Przesyłanie wybranego obrazka do Firebase (Storage)
uploadImageBtn.onclick = () => {
	//3.1 Pobranie nazwy docelowego pliku z inputa
	const fileName = imageName.value;

	//3.2 Złączenie docelowej nazwy pliku z rozszerzeniem pliku
	// Plik musi mieć pełną nazwę, tzn nazwa + rozszerzenie np jpg (nazwa.jpg)
	const fullFileName = `${fileName}.${fileExtension}`;
	// console.log(fullFileName);

	//3.3 Określenie metadanych

	// console.log(fileToUpload);
	const metaData = {
		contentType: fileToUpload.type,
	};

	//3.4 Komunikacja ze Storage- Wrzucenie pliku na serwer
	// uploadBytesResumable(ref(jaki_moduł?, w_jakim_folderzejaki_plik??), jaki_plik?, wskaż_metadane)
	const uploadProcess = uploadBytesResumable(ref(storage, `images/${fullFileName}`), fileToUpload, metaData);

	// 3.5 Wbijamy się w moment kiedy status wrzucania pliku będzie się zmieniał
	//Składnia callback -> Promise -> async/await
	uploadProcess.on(
		'state_changed',
		//Co robić w trakcje przesyłania?
		snapshot => {
			const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
			uploadImageProgressBar.innerHTML = `${Math.round(progress)}%`;
		},
		//Co robić podczas błędu w przesyłaniu?
		error => {
			console.error(error);
		},
		// Co robić, kiedy proces zakończy się sukcesem?
		async () => {
			//3.6 pobranie URL pliku
			const imageUrl = await getDownloadURL(uploadProcess.snapshot.ref);
			// console.log(imageUrl);

			//3.7 Umieszczenie obrazka na stronie
			uploadedImage.setAttribute('src', imageUrl);
		}
	);
};

// ----- ODCZYT PLIKÓW Z SERWERA -----

// 4. Pobranie konkretnego obrazka
//4.1 Pobranie URL obrazka
const imageUrl = await getDownloadURL(ref(storage, 'images/pieski.jpg'));
// 4.2 Umieszczenie obrazka na stronie
uploadedImage.setAttribute('src', imageUrl);

// 5. Pobranie wszystkich dostępnych plików (w folderze)
//5.1 Pobranie nazw plików z konkretnego folderu
// Jako wynik otrzymamy listę
const allImages = await listAll(ref(storage, 'images'));

//5.2 Przejście pętlą prze wszystkie znalezione pliki

// let dropDownView = <select..>?

allImages.items.forEach(async imageRef => {
	const imageUrl = await getDownloadURL(imageRef);
	// Wyświetlanie linków do zdjęć:
	// console.log(imageUrl);


	//Wyświetlanie ścieżek zdjęć:
	// console.log(imageRef._location.path);
});
